<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Employee</title>
        <style>
            <%@include file="css/footer.css"%>
            <%@include file="css/employee_info.css"%>
        </style>
    </head>
    <body>
        <h3>
            Hello, ${employee.name}!
        </h3>
        <p>
            Your personal info:<br>
            <pre class="employee">
                Name: ${employee.name};
                id: ${employee.id};
                role: ${employee.role};
                department: ${employee.department};
                salary: ${employee.salary}$;
                Vacation period: ${employee.vacationPeriod};
                Vacation agreement: ${employee.vacationAgreement}
            </pre><br>
        </p>
        <form action="/company/member/${employee.name}/vacation" method="post">
            I'm so tired, i want to relax...
            <input type="text" name="employee_vacation">
            <input type="submit" value="Request">
        </form>
        <%@include file="footer.html"%>
    </body>
</html>
<%-------------------------------------- Employee page --------------------------------------%>