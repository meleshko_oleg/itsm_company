<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Welcome</title>
        <style>
            <%@include file="css/footer.css"%>
        </style>
    </head>
    <body>
        <h3>
            Hello, guest!
        </h3>
        <p>
            Enter your name:
            <form action="/company/member" method="get">
                <input type="text" name="employee_name">
                <input type="submit" value="Enter">
            </form>
        </p>
        <%@include file="footer.html"%>
    </body>
</html>
<%--------------------------------- Welcome page ---------------------------------%>