package task.constants;

/**
 * jsp - controller attributes.
 */
public class Attributes {
    public static final String EMPLOYEE = "employee";
    public static final String SUBORDINATE_EMPLOYEES = "subordinateEmployees";
    public static final String TIRED_EMPLOYEE = "tired_employee";
    public static final String EMPLOYEE_NAME = "employee_name";
    public static final String EMPLOYEE_VACATION = "employee_vacation";
    public static final String EMPLOYEE_DEPARTMENT = "employee_department";
    public static final String EMPLOYEE_ROLE = "employee_role";
    public static final String EMPLOYEE_ID = "employee_id";
    public static final String EMPLOYEE_SALARY = "employee_salary";
    public static final String VACATION_AGREEMENT = "vacation_agreement";
}