package task.constants;

/**
 * All departments that exist in the company.
 */
public enum Department {
    ADVERTISEMENT,
    MANAGEMENT,
    FINANCIAL,
    HR,
    DEVELOPMENT,
    SALES,
    SUPPORT,
    ADMINISTRATION;
}