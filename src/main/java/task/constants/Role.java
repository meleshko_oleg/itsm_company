package task.constants;

/**
 * All roles that exist in the company.
 */
public enum Role {
    EMPLOYEE,
    CHIEF,
    BOSS
}