package task.dao;

import task.entity.Employee;
import task.constants.Department;
import task.constants.Role;
import java.util.List;

/**
 * Main interface to work with an employee of the company.
 */
public interface EmployeeDao {

    /**
     * Get employee by name from storage.
     * @param employeeName Employee name.
     * @return an instance of the employee.
     */
    Employee getEmployeeByName (String employeeName);

    /**
     * Get all subordinate employees from storage.
     * @param employee Employee (chief or boss).
     * @return list of all subordinate employees.
     */
    List<Employee> getSubordinateEmployees (Employee employee);

    /**
     * Register a vacation period.
     * @param employeeName Employee name.
     * @param vacationPeriod Vacation period (dates).
     */
    void registerVacation (String employeeName, String vacationPeriod);

    /**
     * Accept or refuse an employee vacation period (for chiefs and bosses).
     * @param tiredEmployee Employee name.
     * @param vacationAgreement Message for this employee (yes, no or something else).
     */
    void addVacationAgreement (String tiredEmployee, String vacationAgreement);

    /**
     * Update employee id (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeId New id.
     */
    void setNewEmployeeId(String employeeName, int employeeId);

    /**
     * Update employee role (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeRole New role.
     */
    void setNewEmployeeRole(String employeeName, Role employeeRole);

    /**
     * Update employee department (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeDepartment New department.
     */
    void setNewEmployeeDepartment(String employeeName, Department employeeDepartment);

    /**
     * Update employee salary (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeSalary New salary.
     */
    void setNewEmployeeSalary(String employeeName, int employeeSalary);
}