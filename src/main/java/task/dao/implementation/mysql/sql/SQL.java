package task.dao.implementation.mysql.sql;

/**
 * All queries for working with employees in MySQL database.
 */
public class SQL {
    public static final String GET_EMPLOYEE_BY_NAME =
            "SELECT " +
            "employee_id, " +
            "employee_name, " +
            "employee_role, " +
            "employee_department, " +
            "employee_salary, " +
            "employee_vacation, " +
            "vacation_agreement " +
            "FROM " +
            "management.employees " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String GET_CHIEF_SUBORDINATE_EMPLOYEES =
            "SELECT " +
            "employee_id, " +
            "employee_name, " +
            "employee_role, " +
            "employee_department, " +
            "employee_salary, " +
            "employee_vacation, " +
            "vacation_agreement " +
            "FROM " +
            "management.employees " +
            "WHERE " +
            "employee_department = :employee_department AND employee_role = :employee_role";
    public static final String GET_BOSS_SUBORDINATE_EMPLOYEES =
            "SELECT " +
            "employee_id, " +
            "employee_name, " +
            "employee_role, " +
            "employee_department, " +
            "employee_salary, " +
            "employee_vacation, " +
            "vacation_agreement " +
            "FROM " +
            "management.employees " +
            "WHERE " +
            "employee_role <> :employee_role";
    public static final String REGISTER_EMPLOYEE_VACATION =
            "UPDATE " +
            "management.employees " +
            "SET " +
            "employee_vacation = :employee_vacation " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String ADD_VACATION_AGREEMENT =
            "UPDATE " +
            "management.employees " +
            "SET " +
            "vacation_agreement = :vacation_agreement " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String SET_NEW_EMPLOYEE_ID =
            "UPDATE " +
            "management.employees " +
            "SET " +
            "employee_id = :employee_id " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String SET_NEW_EMPLOYEE_ROLE =
            "UPDATE " +
            "management.employees " + "SET " +
            "employee_role = :employee_role " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String SET_NEW_EMPLOYEE_DEPARTMENT =
            "UPDATE " +
            "management.employees " +
            "SET " +
            "employee_department = :employee_department " +
            "WHERE " +
            "employee_name = :employee_name";
    public static final String SET_NEW_EMPLOYEE_SALARY =
            "UPDATE " +
            "management.employees " +
            "SET " +
            "employee_salary = :employee_salary " +
            "WHERE " +
            "employee_name = :employee_name";
}