package task.dao.implementation.mysql.connection;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import java.util.ResourceBundle;

/**
 * Connection to MySQL database.
 */
@Component
public class DBManager {
    private static final ResourceBundle MYSQL_INFO = ResourceBundle.getBundle(ResourceBundleKeys.MYSQL);    // from file MySQL.properties
    private JdbcTemplate jdbcTemplate;

    public DBManager() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(MYSQL_INFO.getString(ResourceBundleKeys.DRIVER));
        dataSource.setUrl(MYSQL_INFO.getString(ResourceBundleKeys.URL));
        dataSource.setUsername(MYSQL_INFO.getString(ResourceBundleKeys.USER_NAME));
        dataSource.setPassword(MYSQL_INFO.getString(ResourceBundleKeys.USER_PASSWORD));
        this.jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * All keys for MySQL.properties file.
     */
    private class ResourceBundleKeys{
        private static final String MYSQL = "MySQL";
        private static final String DRIVER = "driver";
        private static final String URL = "url";
        private static final String USER_NAME = "username";
        private static final String USER_PASSWORD = "userpassword";
    }
}