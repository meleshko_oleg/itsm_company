package task.dao.implementation.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import task.constants.Attributes;
import task.dao.EmployeeDao;
import task.dao.implementation.mysql.connection.DBManager;
import task.dao.implementation.mysql.sql.SQL;
import task.entity.Employee;
import task.constants.Department;
import task.constants.Role;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of EmployeeDao interface using MySQL database as employee storage.
 */
@Component
public class EmployeeDaoDB implements EmployeeDao{
    @Autowired
    private DBManager dbManager;

    /**
     * Get employee by name from database.
     * @param employeeName Employee name.
     * @return employee from database using employee name.
     */
    @Override
    public Employee getEmployeeByName(String employeeName){
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        Employee employee = namedParameterJdbcTemplate.queryForObject(
                SQL.GET_EMPLOYEE_BY_NAME,
                new MapSqlParameterSource(EMPLOYEE_NAME_COLUMN_NAME, employeeName),
                new EmployeeMapper());
        return employee;
    }

    /**
     * Get all subordinate employees from database.
     * @param employee Employee (chief or boss).
     * @return list of subordinate employees.
     */
    @Override
    public List<Employee> getSubordinateEmployees(Employee employee){
        Role employeeRole = employee.getRole();
        Department employeeDepartment = employee.getDepartment();

        List<Employee> subordinateEmployees = getSubordinateEmployees(employeeRole, employeeDepartment);
        return subordinateEmployees;
    }

    /**
     * Register a vacation period.
     * @param employeeName Employee name.
     * @param employeeVacation Vacation period (dates).
     */
    @Override
    public void registerVacation(String employeeName, String employeeVacation) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYEE_VACATION_COLUMN_NAME = Attributes.EMPLOYEE_VACATION;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, employeeName);
        mapSqlParameterSource.addValue(EMPLOYEE_VACATION_COLUMN_NAME, employeeVacation);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.REGISTER_EMPLOYEE_VACATION, mapSqlParameterSource);
    }

    /**
     * Additional method for getting subordinate employees for chiefs or bosses.
     * @param role Chief / boss.
     * @param department ADVERTISEMENT / MANAGEMENT / FINANCIAL / HR / DEVELOPMENT / SALES / SUPPORT
     * @return list of specific subordinate employees.
     */
    private List<Employee> getSubordinateEmployees(Role role, Department department){
        List<Employee> subordinateEmployees = null;
        switch (role){
            case CHIEF:
                subordinateEmployees = getChiefSubordinateEmployees(department);
                break;
            case BOSS:
                subordinateEmployees = getBossSubordinateEmployees();
                break;
            case EMPLOYEE:
                break;
        }
        return subordinateEmployees;
    }

    /**
     * Additional method for getting subordinate employees for chiefs.
     * @param employeeDepartment ADVERTISEMENT / MANAGEMENT / FINANCIAL / HR / DEVELOPMENT / SALES / SUPPORT
     * @return list of chief's subordinate employees.
     */
    private List<Employee> getChiefSubordinateEmployees(Department employeeDepartment) {
        final String EMPLOYEE_DEPARTMENT_COLUMN_NAME = Attributes.EMPLOYEE_DEPARTMENT;
        final String EMPLOYEE_ROLE_COLUMN_NAME = Attributes.EMPLOYEE_ROLE;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_DEPARTMENT_COLUMN_NAME, employeeDepartment.name());
        mapSqlParameterSource.addValue(EMPLOYEE_ROLE_COLUMN_NAME, Role.EMPLOYEE.name());

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        List<Employee> subordinateEmployees = namedParameterJdbcTemplate.query(
                SQL.GET_CHIEF_SUBORDINATE_EMPLOYEES,
                mapSqlParameterSource,
                new EmployeeMapper());

        return subordinateEmployees;
    }

    /**
     * Additional method for getting subordinate employees for bosses.
     * @return list of boss' subordinate employees.
     */
    private List<Employee> getBossSubordinateEmployees(){
        final String EMPLOYEE_ROLE_COLUMN_NAME = Attributes.EMPLOYEE_ROLE;

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        List<Employee> subordinateEmployees = namedParameterJdbcTemplate.query(
                SQL.GET_BOSS_SUBORDINATE_EMPLOYEES,
                new MapSqlParameterSource(EMPLOYEE_ROLE_COLUMN_NAME, Role.BOSS.name()),
                new EmployeeMapper()
        );
        return subordinateEmployees;
    }

    /**
     * Accept or refuse an employee vacation period (for chiefs and bosses).
     * @param tiredEmployee Employee name.
     * @param vacationAgreement Message for this employee (yes, no or something else).
     */
    @Override
    public void addVacationAgreement(String tiredEmployee, String vacationAgreement) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYEE_VACATION_AGREEMENT_COLUMN_NAME = Attributes.VACATION_AGREEMENT;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, tiredEmployee);
        mapSqlParameterSource.addValue(EMPLOYEE_VACATION_AGREEMENT_COLUMN_NAME, vacationAgreement);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.ADD_VACATION_AGREEMENT, mapSqlParameterSource);
    }


    /**
     * Update employee id (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeId New id.
     */
    @Override
    public void setNewEmployeeId(String employeeName, int employeeId) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYEE_ID_COLUMN_NAME = Attributes.EMPLOYEE_ID;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_ID_COLUMN_NAME, employeeId);
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, employeeName);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.SET_NEW_EMPLOYEE_ID, mapSqlParameterSource);
    }

    /**
     * Update employee role (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeRole New role.
     */
    @Override
    public void setNewEmployeeRole(String employeeName, Role employeeRole) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYEE_ROLE_COLUMN_NAME = Attributes.EMPLOYEE_ROLE;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_ROLE_COLUMN_NAME, employeeRole.name());
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, employeeName);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.SET_NEW_EMPLOYEE_ROLE, mapSqlParameterSource);
    }

    /**
     * Update employee department (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeDepartment New department.
     */
    @Override
    public void setNewEmployeeDepartment(String employeeName, Department employeeDepartment) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYEE_DEPARTMENT_COLUMN_NAME = Attributes.EMPLOYEE_DEPARTMENT;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYEE_DEPARTMENT_COLUMN_NAME, employeeDepartment.name());
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, employeeName);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.SET_NEW_EMPLOYEE_DEPARTMENT, mapSqlParameterSource);

    }

    /**
     * Update employee salary (powers of bosses).
     * @param employeeName Employee name.
     * @param employeeSalary New salary.
     */
    @Override
    public void setNewEmployeeSalary(String employeeName, int employeeSalary) {
        final String EMPLOYEE_NAME_COLUMN_NAME = Attributes.EMPLOYEE_NAME;
        final String EMPLOYE_SALARY_COLUMN_NAME = Attributes.EMPLOYEE_SALARY;

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(EMPLOYE_SALARY_COLUMN_NAME, employeeSalary);
        mapSqlParameterSource.addValue(EMPLOYEE_NAME_COLUMN_NAME, employeeName);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbManager.getJdbcTemplate());
        namedParameterJdbcTemplate.update(SQL.SET_NEW_EMPLOYEE_SALARY, mapSqlParameterSource);
    }

    /**
     * Class that helps to work with employee in database.
     */
    private class EmployeeMapper implements RowMapper<Employee>{
        private static final int EMPLOYEE_ID_COLUMN_INDEX = 1;
        private static final int EMPLOYEE_NAME_COLUMN_INDEX = 2;
        private static final int EMPLOYEE_ROLE_COLUMN_INDEX = 3;
        private static final int EMPLOYEE_DEPARTMENT_COLUMN_INDEX = 4;
        private static final int EMPLOYEE_SALARY_COLUMN_INDEX = 5;
        private static final int EMPLOYEE_VACATION_COLUMN_INDEX = 6;
        private static final int EMPLOYEE_VACATION_AGREEMENT_COLUMN_INDEX = 7;

        public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
            Employee employee = new Employee();
            employee.setId(resultSet.getInt(EMPLOYEE_ID_COLUMN_INDEX));
            employee.setName(resultSet.getString(EMPLOYEE_NAME_COLUMN_INDEX));
            employee.setRole(Role.valueOf(resultSet.getString(EMPLOYEE_ROLE_COLUMN_INDEX)));
            employee.setDepartment(Department.valueOf(resultSet.getString(EMPLOYEE_DEPARTMENT_COLUMN_INDEX)));
            employee.setSalary(resultSet.getInt(EMPLOYEE_SALARY_COLUMN_INDEX));
            employee.setVacationPeriod(resultSet.getString(EMPLOYEE_VACATION_COLUMN_INDEX));
            employee.setVacationAgreement(resultSet.getString(EMPLOYEE_VACATION_AGREEMENT_COLUMN_INDEX));
            return employee;
        }
    }
}