package task.factories;

import task.entity.Employee;
import task.constants.Role;

/**
 * Find *.jsp files.
 */
public class ViewFactory {

    /**
     * Factory to find *.jsp file depending on employee role.
     * @param employee Employee of the company.
     * @return a representation using .jsp file.
     */
    public static String getViewByEmployeeRole(Employee employee){
        String viewName = "";
        Role role = employee.getRole();
        switch (role){
            case EMPLOYEE:
                viewName = "employee";
                break;
            case CHIEF:
                viewName = "chief";
                break;
            case BOSS:
                viewName = "boss";
                break;
        }
        return viewName;
    }
}