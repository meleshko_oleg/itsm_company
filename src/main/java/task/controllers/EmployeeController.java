package task.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import task.constants.Attributes;
import task.dao.EmployeeDao;
import task.entity.Employee;
import task.factories.ViewFactory;
import java.util.List;

/**
 * Main controller to work with all employees.
 */
@Controller
@RequestMapping("/company")
public class EmployeeController {
    @Autowired
    EmployeeDao employeeDao;

    /**
     * Get info of one employee by name.
     * @param employeeName Name.
     */
    @RequestMapping (value = "/member", method = RequestMethod.GET)
    String oneEmployee(@RequestParam (Attributes.EMPLOYEE_NAME) String employeeName, Model model){
        Employee employee = employeeDao.getEmployeeByName(employeeName);
        String vieName = ViewFactory.getViewByEmployeeRole(employee);
        List<Employee> subordinateEmployees = employeeDao.getSubordinateEmployees(employee);
        model.addAttribute(Attributes.EMPLOYEE, employee);
        model.addAttribute(Attributes.SUBORDINATE_EMPLOYEES, subordinateEmployees);
        return vieName;
    }

    /**
     * Register employee vacation period.
     * @param employeeName Name of employee.
     * @param employeeVacation Period.
     */
    @RequestMapping (value = "member/{employeeName}/vacation", method = RequestMethod.POST)
    ModelAndView registerVacation(@PathVariable String employeeName, @RequestParam (Attributes.EMPLOYEE_VACATION) String employeeVacation){
        employeeDao.registerVacation(employeeName, employeeVacation);
        return new ModelAndView("redirect:/company/member", Attributes.EMPLOYEE_NAME, employeeName);
    }

    /**
     * Accept or refuse an employee vacation period (for chiefs and bosses).
     * @param tiredEmployee Employee name.
     * @param vacationAgreement Message to employee (yes, no or something else).
     * @param employeeName Name of the chief (boss).
     */
    @RequestMapping (value = "/member/vacation_agreement", method = RequestMethod.POST)
    ModelAndView addVacationAgreement(
                                        @RequestParam (Attributes.TIRED_EMPLOYEE) String tiredEmployee,
                                        @RequestParam (Attributes.VACATION_AGREEMENT) String vacationAgreement,
                                        @RequestParam (Attributes.EMPLOYEE_NAME) String employeeName){
        employeeDao.addVacationAgreement(tiredEmployee, vacationAgreement);
        return new ModelAndView("redirect:/company/member", Attributes.EMPLOYEE_NAME, employeeName);
    }
}