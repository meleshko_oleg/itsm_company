package task.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import task.constants.Attributes;
import task.dao.EmployeeDao;
import task.entity.Employee;
import task.constants.Department;
import task.constants.Role;

/**
 * Controller for employee editing.
 */
@Controller
@RequestMapping("/edit/member")
public class EditEmployeeController {
    @Autowired
    EmployeeDao employeeDao;

    /**
     * Get employee info page with possibility to change it.
     * @param employeeName Name of an employee.
     */
    @RequestMapping (value = "/{employeeName}", method = RequestMethod.GET)
    ModelAndView getEmployeeEditor(@PathVariable String employeeName){
        Employee employee = employeeDao.getEmployeeByName(employeeName);
        return new ModelAndView("editor", Attributes.EMPLOYEE, employee);
    }

    /**
     * Set new id of the employee.
     * @param employeeName Name of an employee.
     * @param employeeId New id.
     */
    @RequestMapping (value = "/{employeeName}/id", method = RequestMethod.POST)
    String setNewEmployeeId(@PathVariable String employeeName, @RequestParam(Attributes.EMPLOYEE_ID) String employeeId){
        employeeDao.setNewEmployeeId(employeeName, Integer.parseInt(employeeId));
        return "redirect:/edit/member/" + employeeName;
    }

    /**
     * Set new role of the employee.
     * @param employeeName Name of an employee.
     * @param employeeRole New role.
     */
    @RequestMapping (value = "/{employeeName}/role", method = RequestMethod.POST)
    String setNewEmployeeRole(@PathVariable String employeeName, @RequestParam(Attributes.EMPLOYEE_ROLE) String employeeRole){
        employeeDao.setNewEmployeeRole(employeeName, Role.valueOf(employeeRole));
        return "redirect:/edit/member/" + employeeName;
    }

    /**
     * Set new department of the employee.
     * @param employeeName Name of an employee.
     * @param employeeDepartment New department.
     */
    @RequestMapping (value = "/{employeeName}/department", method = RequestMethod.POST)
    String setNewEmployeeDepartment(@PathVariable String employeeName, @RequestParam(Attributes.EMPLOYEE_DEPARTMENT) String employeeDepartment){
        employeeDao.setNewEmployeeDepartment(employeeName, Department.valueOf(employeeDepartment));
        return "redirect:/edit/member/" + employeeName;
    }

    /**
     * Set new salary of the employee.
     * @param employeeName Name of an employee.
     * @param employeeSalary New salary.
     * @return
     */
    @RequestMapping (value = "/{employeeName}/salary", method = RequestMethod.POST)
    String setNewEmployeeSalary(@PathVariable String employeeName, @RequestParam(Attributes.EMPLOYEE_SALARY) String employeeSalary){
        employeeDao.setNewEmployeeSalary(employeeName, Integer.parseInt(employeeSalary));
        return "redirect:/edit/member/" + employeeName;
    }
}