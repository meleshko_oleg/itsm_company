package task.entity;

import task.constants.Department;
import task.constants.Role;

/**
 * Main entity of the company.
 */
public class Employee {
    private int id;
    private String name;
    private Role role;
    private Department department;
    private int salary;
    private String vacationPeriod;
    private String vacationAgreement;

    public Employee() {
    }

    public Employee(int id, String name, Role role, Department department, int salary, String vacationPeriod, String vacationAgreement) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.department = department;
        this.salary = salary;
        this.vacationPeriod = vacationPeriod;
        this.vacationAgreement = vacationAgreement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getVacationPeriod() {
        return vacationPeriod;
    }

    public void setVacationPeriod(String vacationPeriod) {
        this.vacationPeriod = vacationPeriod;
    }

    public String getVacationAgreement() {
        return vacationAgreement;
    }

    public void setVacationAgreement(String vacationAgreement) {
        this.vacationAgreement = vacationAgreement;
    }
}