<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Editor</title>
        <style>
            <%@include file="css/editor.css"%>
        </style>
    </head>
    <body>
        <h3>
            Name: ${employee.name}
        </h3>

        <span>Id: ${employee.id}</span><br>
        <form action="/edit/member/${employee.name}/id" method="post">
            new id:
            <input type="text" name="employee_id">
            <input type="submit" value="Edit">
        </form>

        <span>Role: ${employee.role}</span><br>
        <form action="/edit/member/${employee.name}/role" method="post">
            new role:
            <input type="text" name="employee_role">
            <input type="submit" value="Edit">
        </form>

        <span>Department: ${employee.department}</span><br>
        <form action="/edit/member/${employee.name}/department" method="post">
            new department:
            <input type="text" name="employee_department">
            <input type="submit" value="Edit">
        </form>

        <span>Salary: ${employee.salary}$</span><br>
        <form action="/edit/member/${employee.name}/salary" method="post">
            new salary:
            <input type="text" name="employee_salary">
            <input type="submit" value="Edit">
        </form>
    </body>
</html>
<%--------------------------------- Edit employee info ---------------------------------%>