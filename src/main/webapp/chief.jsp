<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Chief</title>
        <style>
            <%@include file="css/footer.css"%>
            <%@include file="css/employee_info.css"%>
            <%@include file="css/table.css"%>
        </style>
    </head>
    <body>
        <h3>
            Hello, ${employee.name}!
        </h3>
        <p>
            Your personal info:<br>
            <pre class="employee">
                Name: ${employee.name};
                id: ${employee.id};
                role: ${employee.role};
                department: ${employee.department};
                salary: ${employee.salary}$;
                Vacation period: ${employee.vacationPeriod};
                Vacation agreement: ${employee.vacationAgreement}
            </pre>
        </p>
        <form action="/company/member/${employee.name}/vacation" method="post">
            My preferable vacation period:
            <input type="text" name="employee_vacation">
            <input type="submit" value="Send">
        </form>
        <table border="1" cellpadding="10">
            <caption align="center">All subordinate employees:</caption>
            <tr bgcolor="gray">
                <td width=10%>Id</td>
                <td>Name</td>
                <td>Role</td>
                <td>Department</td>
                <td>Salary</td>
                <td width="30%">Vacation</td>
                <td>Vacation agreement</td>
            </tr>
            <c:forEach var="employee" items="${subordinateEmployees}" varStatus="Count">
                <tr>
                    <td>${employee.id}</td>
                    <td>${employee.name}</td>
                    <td>${employee.role}</td>
                    <td>${employee.department}</td>
                    <td>${employee.salary}$</td>
                    <td>${employee.vacationPeriod}</td>
                    <td>${employee.vacationAgreement}</td>
                </tr>
            </c:forEach>
        </table>
        <form action="/company/member/vacation_agreement" method="post">
            Vacation management:
            <input type="text" name="tired_employee" placeholder="employee name">
            <input type="text" name="vacation_agreement" placeholder="vacation info">
            <input type="hidden" name="employee_name" value="${employee.name}">
            <input type="submit" value="Apply">
        </form>
        <%@include file="footer.html"%>
    </body>
</html>
<%-------------------------------------- Chief page --------------------------------------%>